import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { PdfViewerPage } from "./../pages/pdf-viewer/pdf-viewer";
import { FileTransfer } from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { SocialSharing } from "@ionic-native/social-sharing";
@NgModule({
  declarations: [MyApp, HomePage, PdfViewerPage],
  imports: [BrowserModule, IonicModule.forRoot(MyApp), PdfViewerModule],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage, PdfViewerPage],
  providers: [
    StatusBar,
    SplashScreen,
    FileTransfer,
    File,
    SocialSharing,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
