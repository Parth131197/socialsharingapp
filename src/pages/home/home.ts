import { Component } from "@angular/core";
import { Modal, ModalController, NavController, Platform } from "ionic-angular";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { PdfViewerPage } from "./../pdf-viewer/pdf-viewer";
import { File } from "@ionic-native/file";
import { SocialSharing } from "@ionic-native/social-sharing";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public platform: Platform,
    private transfer: FileTransfer,
    private file: File,
    private socialSharing: SocialSharing
  ) {}

  shareFile(url) {
    console.log("Inside Share File");
    this.socialSharing
      .share("Message", "Subject", url, "myFile.pdf")
      .then(entries => {
        console.log("success " + JSON.stringify(entries));
      })
      .catch(error => {
        alert("error " + JSON.stringify(error));
      });
  }

  getPath() {
    let path = null;
    if (this.platform.is("ios")) {
      path = this.file.documentsDirectory;
    } else {
      path = this.file.dataDirectory;
    }
  }
  downloadAndShare(): void {
    console.log("Inside open");
    let path = this.file.dataDirectory; //this.getPath();  //creating an error
    const transfer = this.transfer.create();
    transfer
      .download(
        "https://devdactic.com/html/5-simple-hacks-LBT.pdf", // link to download the file
        path + "myFile.pdf"
      )
      .then(entry => {
        let url = entry.toURL();
        console.log("Saved to:", url);
        this.shareFile(url);
      });
  }
}
